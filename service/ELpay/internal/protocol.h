/* 
 * File:   protocol.h
 */

#include <stddef.h>
#include "json.h"

#ifndef ELPAY_PROTOCOL_H
#define ELPAY_PROTOCOL_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
  const char * model;
  const char * os_ver;
  const char * imei;
  const char * phone_num;
  const char * lac;
  const char * cid;
} ELpayMVI_t;

#define ELPAY_JSON_KEYenc_SIZE 12
#define ELPAY_JSON_KEYenc_BUFSIZE (ELPAY_JSON_KEYenc_SIZE+1)
typedef char ELpayKeyEnc_t[ELPAY_JSON_KEYenc_BUFSIZE];

#define ELPAY_JSON_KEYSIZE 8
#define ELPAY_JSON_KEY_BUFSIZE (ELPAY_JSON_KEYSIZE+1)
typedef char ELpayKey_t[ELPAY_JSON_KEY_BUFSIZE];

typedef struct
{
  ELpayKey_t code;  
  ELpayMVI_t mvi;
  const char * pos_os_ver;
  const char * app_ver;
} ELpayRegData_t;

typedef struct
{
  const char * login;
  const char * password;
  
  const char * termId;
  const char * pos_os_ver;
  const char * app_ver;
  ELpayMVI_t mvi;
} ELpayAuthData_t;

typedef struct
{
  const char * termId;
  const char * session;
  const char * app_ver;
} ELpayBaseRequestData_t;

typedef struct
{
  ELpayBaseRequestData_t base;
} ELpayBalanceData_t;

typedef struct
{
  ELpayBaseRequestData_t base;
} ELpayProductsListData_t;

typedef struct
{
  ELpayBaseRequestData_t base;
  const char * name;
} ELpayReceiptTemplateData_t;

typedef struct
{
  ELpayBaseRequestData_t base;
  long commision;
  const char * msisdn;
  long pid;
  long act;
  long nominal;
} ELpayCreateOperationData_t;

typedef struct
{
  ELpayBaseRequestData_t base;
  long tranId;
} ELpayConfirmOperationData_t;

typedef struct
{
  ELpayBaseRequestData_t base;
  long tranId;
} ELpayOperationStatusData_t;

#define ELPAY_JSON_TERM_IDSIZE 16
#define ELPAY_JSON_TERM_ID_BUFSIZE (ELPAY_JSON_TERM_IDSIZE+1)
typedef char ELpayTermId_t[ELPAY_JSON_TERM_ID_BUFSIZE];

#define ELPAY_JSON_SESSION_IDSIZE 32
#define ELPAY_JSON_SESSION_ID_BUFSIZE (ELPAY_JSON_SESSION_IDSIZE+1)
typedef char ELpaySessionId_t[ELPAY_JSON_SESSION_ID_BUFSIZE];

#define ELPAY_JSON_AUTH_BUFSIZE (32+1)
typedef char ELpayAuthBuf_t[ELPAY_JSON_AUTH_BUFSIZE];

typedef struct 
{
  ELpayAuthBuf_t login;
  ELpayAuthBuf_t password;
  ELpayTermId_t id;
  ELpayKey_t sign_key;
  ELpaySessionId_t session;
  long long tranId;
} ELpayTerminalData_t;

char * elpay_sign(const char * key, char * packet);

size_t elpay_reg_req_gen
(
  ELpayRegData_t* data,
  char * buf, size_t size
);
int elpay_auth_req_gen
(
  ELpayAuthData_t* data,
  char * buf, size_t size
);
int elpay_balance_req_gen
(
  ELpayBalanceData_t* data,
  char * buf, size_t size
);
int elpay_products_list_req_gen
(
  ELpayProductsListData_t* data,
  char * buf, size_t size
);
int elpay_receipt_template_req_gen
(
  ELpayReceiptTemplateData_t* data,
  char * buf, size_t size
);
int elpay_create_operation_req_gen
(
  ELpayCreateOperationData_t* data,
  char * buf, size_t size);
int elpay_confirm_operation_req_gen
(
  ELpayConfirmOperationData_t* data,
  char * buf, size_t size
);
int elpay_operation_status_req_gen
(
  ELpayOperationStatusData_t* data,
  char * buf, size_t size
);

int elpay_reg_resp_parse
(
  const char * buf, 
  ELpayKey_t regcode,
  ELpayTerminalData_t * res,
  char * errbuf, size_t errbufSize
);
int elpay_auth_resp_parse
(
  const char * buf, 
  ELpayTerminalData_t * res,
  char * errbuf, size_t errbufSize
);
int elpay_create_operation_resp_parse
(
  const char * buf, 
  ELpayTerminalData_t * res,
  char * errbuf, size_t errbufSize
);

#ifdef __cplusplus
}
#endif

#endif /* ELPAY_PROTOCOL_H */

