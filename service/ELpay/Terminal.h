/* 
 * File:   Terminal.h
 */

#ifndef ELPAY_TERMINAL_H
#define ELPAY_TERMINAL_H

#include <string>
#include "internal/operationStatus.h"

#define TERMINAL_QUEUE_NAME "ELpayTerminalQueue"
#define TERMINAL_QUEUE_MAXCOUNT 10
#define TERMINAL_QUEUE_MSGSIZE sizeof(TerminalMsg)

#define TERMINAL_RES_QUEUE_NAME "ELpayTerminalResultQueue"
#define TERMINAL_RES_QUEUE_MAXCOUNT 1
#define TERMINAL_RES_QUEUE_MSGSIZE sizeof(TerminalResultMsg)

typedef char TerminalReceiptTemplateNameBuf_t[32+1];

struct TerminalMsg
{
  enum Action_t
  {
    Exit = 0,
    ELpay_Reg,
    ELpay_Auth,
    ELpay_ProductsList,
    ELpay_ReceiptTemplate,
    ELpay_CreateOperation,
    ELpay_ConfirmOperation,
    ELpay_OperationStatus,
    ELpay_Balance
  } action;
  union Data
  {
    struct
    {
      long code;
    } reg;
    TerminalReceiptTemplateNameBuf_t recTemplName;
    struct
    {
      long pid;
    } createOp;
  } data;
};

struct TerminalResultMsg
{
  ELpayOperationStatus_t status;
};

void init_Terminal();
void free_Terminal();

void Terminal_exit();

void Terminal_setId(const char * id);
bool Terminal_setSignKey(const char * sign_key_base64);
bool Terminal_setAuthData(std::string & data);

void Terminal_elpay_reg(long code);
void Terminal_elpay_auth();
void Terminal_elpay_getBalance();
void Terminal_elpay_getProductsList();
void Terminal_elpay_getReceiptTemplate(const char * name);
void Terminal_elpay_CreateOperation(long pid);
void Terminal_elpay_ConfirmOperation();
void Terminal_elpay_OperationStatus();

bool Terminsl_getResultMsg(TerminalResultMsg * msg);
bool Terminsl_getResultStr(std::string * msg);

#endif /* ELPAY_TERMINAL_H */

