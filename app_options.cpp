#include <boost/program_options.hpp>

#include "public.h"
#include "app_options.h"

#include "system/logs.h"
#include "service/ELpay/Terminal.h"

using namespace boost;
using namespace boost::program_options;

#define APP_OPT_HELP "help"
#define APP_OPT_DUMMY_SERVER "DummyServer"
#define APP_OPT_EXIT "auto-exit"
#define APP_OPT_VERBOSE "verbose"

#define APP_OPT_ELPAY_TERM "elpay-term"
#define APP_OPT_ELPAY_SK "elpay-sk"

#define APP_OPT_ELPAY_REG "elpay-reg"
#define APP_OPT_ELPAY_AUTH "elpay-auth"

appCommand_t app_options_handler(int argc, char** argv, menuAction_t* menuAction)
{
  options_description general("General options");
  general.add_options()
    (APP_OPT_HELP",h", "Show help")
    (APP_OPT_DUMMY_SERVER",D", "Using dummy server for local debug")
    (APP_OPT_EXIT",e", "exit after operation comleate")
    (APP_OPT_VERBOSE",v", "enable low-lewel data output")
  ;
  
  options_description descELpay("ELpay options");
  descELpay.add_options()
    (APP_OPT_ELPAY_REG",r", value<long>(), "registration with code <arg>")
    (APP_OPT_ELPAY_TERM",t", value<std::string>(), "set terminal id for operations")
    (APP_OPT_ELPAY_SK",k", value<std::string>(), "set sign key for operations")
    (APP_OPT_ELPAY_AUTH",a", value<std::string>(), "authorization with <arg> = <login>:<password>")
  ;  
  options_description desc;
  desc.add(general).add(descELpay);  

  parsed_options parsed = command_line_parser(argc, argv)
    .options(desc).allow_unregistered().run();
  
  variables_map varsMap;
  store(parsed, varsMap);
  notify(varsMap);
  
  if (varsMap.count(APP_OPT_HELP))
    return app_EXIT;
 
  appCommand_t cmd = app_CONTINUE;
  
  if (varsMap.count(APP_OPT_DUMMY_SERVER))
    flag_DummyServer_Enable = true;
  
  if (varsMap.count(APP_OPT_EXIT))
    flag_AutoExit = true;
  
  if (varsMap.count(APP_OPT_VERBOSE))
    flag_stdout_Verbose = true;

  if (varsMap.count(APP_OPT_ELPAY_REG))
  {
    flag_readAction_enable = false;
    *menuAction = menu_ELpay_Reg;
    elpayRCode = varsMap[APP_OPT_ELPAY_REG].as<long>();
  }
  
  if (varsMap.count(APP_OPT_ELPAY_TERM))
  {
    std::string str;
    str = varsMap[APP_OPT_ELPAY_TERM].as<std::string>();
    Terminal_setId(str.data());
  }

  if (varsMap.count(APP_OPT_ELPAY_SK))
  {
    std::string str;
    str = varsMap[APP_OPT_ELPAY_SK].as<std::string>();
    Terminal_setSignKey(str.data());
  }
  
  if (varsMap.count(APP_OPT_ELPAY_AUTH))
  {
    flag_readAction_enable = false;
    *menuAction = menu_ELpay_Auth;
    std::string str;
    str = varsMap[APP_OPT_ELPAY_AUTH].as<std::string>();
    Terminal_setAuthData(str);
  }
  
  //----------------------------------------------------------------------------
  
  if (flag_DummyServer_Enable)
  {
    ipPoint_json_Addr = DymmyServer_json_Addr;
    flag_Server_ResolveAddr = 0;
  }
  else
  {
    ipPoint_json_Addr = Server_json_Addr;
    flag_Server_ResolveAddr = 1;
  }
  
  return cmd;
}
