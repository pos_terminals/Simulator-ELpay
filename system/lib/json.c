/* 
 * File:   json.c
 */

#include "json.h"

#include <stdio.h>
#include <string.h>

/* non-zero when we're reformatting a stream */
static int s_streamReformat = 1;

#define GENnva(__stat,func,gen)                                     \
{                                                                   \
  __stat = func(gen);                                               \
  if (__stat == yajl_gen_generation_complete && s_streamReformat) { \
    yajl_gen_reset(gen, "\n");                                      \
    __stat = func(gen);                                             \
  }                                                                 \
}

#define GEN(__stat,func,gen,...)                                    \
{                                                                   \
  __stat = func(gen,__VA_ARGS__);                                   \
  if (__stat == yajl_gen_generation_complete && s_streamReformat) { \
    yajl_gen_reset(gen, "\n");                                      \
    __stat = func(gen,__VA_ARGS__);                                 \
  }                                                                 \
}

#define GEN_CHECK_STATUS(header,stat,res)                           \
{                                                                   \
  res = (stat == yajl_gen_status_ok);                               \
  if (!res)                                                         \
    printf("WRN: %s: failure, status=%d\n",header,stat);            \
}

#define GENnva_AND_WARNING(func,res,gen)                            \
{                                                                   \
  yajl_gen_status __stat;                                           \
  GENnva(__stat,func,gen);                                          \
  GEN_CHECK_STATUS(#func,__stat,res);                               \
}

#define GEN_AND_WARNING(func,res,gen,...)                           \
{                                                                   \
  yajl_gen_status __stat;                                           \
  GEN(__stat,func,gen,__VA_ARGS__);                                 \
  GEN_CHECK_STATUS(#func,__stat,res);                               \
  }

#define TYPE_GENnva_AND_WARNING(type,res,gen) GENnva_AND_WARNING(yajl_gen_ ## type,res,gen)
#define TYPE_GEN_AND_WARNING(type,res,gen,...) GEN_AND_WARNING(yajl_gen_ ## type,res,gen,__VA_ARGS__)

static inline yajl_gen_status yajl_gen_STRING(yajl_gen hand, const JSON_CHAR * str)
{
  return yajl_gen_string(hand, str, strlen(str));
}

//-------------------------------------------------------------

void init_jsonGen(jsonGen_t* This)
{
  This->Generator = yajl_gen_alloc(NULL);
//  yajl_gen_config(This->Generator, yajl_gen_beautify, 1);
  yajl_gen_config(This->Generator, yajl_gen_validate_utf8, 1);
}

void free_jsonGen(jsonGen_t* This)
{
  yajl_gen_free(This->Generator);
}

void jsonGen_clear(jsonGen_t* This)
{
  yajl_gen_clear(This->Generator);
}

int json_gen_object_open(jsonGen_t* This, const JSON_CHAR * name)
{
  int res;
  if (name)
  {
    TYPE_GEN_AND_WARNING(STRING,res, This->Generator, name);
    if (!res) return 0;
  }    
  GENnva_AND_WARNING(yajl_gen_map_open,res,This->Generator);
  return res;
}

int json_gen_object_close(jsonGen_t* This)
{
  int res;
  GENnva_AND_WARNING(yajl_gen_map_close,res,This->Generator);
  return res;
}

int json_gen_array_open(jsonGen_t* This, const JSON_CHAR * name)
{
  int res;
  if (name)
  {
    TYPE_GEN_AND_WARNING(STRING,res, This->Generator, name);
    if (!res) return 0;
  }    
  GENnva_AND_WARNING(yajl_gen_array_open,res,This->Generator);
  return res;
}

int json_gen_array_close(jsonGen_t* This)
{
  int res;
  GENnva_AND_WARNING(yajl_gen_array_close,res,This->Generator);
  return res;
}

int json_gen_write_null(jsonGen_t* This, const JSON_CHAR * name)
{
  int res;
  TYPE_GEN_AND_WARNING(STRING,res, This->Generator, name);
  if (!res) return 0;
  TYPE_GENnva_AND_WARNING(null,res, This->Generator);
  return res;
}

int json_gen_write_str(jsonGen_t* This, const JSON_CHAR * name, const JSON_CHAR * str)
{
  int res;
  TYPE_GEN_AND_WARNING(STRING,res, This->Generator, name);
  if (!res) return 0;
  TYPE_GEN_AND_WARNING(STRING,res, This->Generator, str);
  return res;
}

int json_gen_write_llint(jsonGen_t* This, const JSON_CHAR * name, long long val)
{
  int res;
  TYPE_GEN_AND_WARNING(STRING,res, This->Generator, name);
  if (!res) return 0;
  TYPE_GEN_AND_WARNING(integer,res, This->Generator, val);
  return res;
}

int jsonGen_internal_buf(jsonGen_t* This, const JSON_CHAR ** buf, size_t * len)
{
  int res;
  GEN_AND_WARNING(yajl_gen_get_buf,res, This->Generator, buf, len);
  return res;
}

size_t jsonGen_copy_to_buf(jsonGen_t* This, char * buf, size_t size)
{
  int res;
  const JSON_CHAR * int_buf; size_t len;
  GEN_AND_WARNING(yajl_gen_get_buf,res, This->Generator, &int_buf, &len);
  if (len > size)
    return 0;
  
  strncpy(buf, int_buf, len);
  buf[len] = '\0';
  return len;
}
