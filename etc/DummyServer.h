/* 
 * File:   DummyServer.h
 */

#ifndef DUMMYSERVER_H
#define DUMMYSERVER_H

#define DUMMYSERVER_QUEUE_NAME "DummyServerQueue"
#define DUMMYSERVER_QUEUE_MAXCOUNT 10
#define DUMMYSERVER_QUEUE_MSGSIZE sizeof(DummyServerMsg)

enum DummyServerAcceptOperation_t
{
  dummy_accept_NoOperation = 0,
  dummy_accept_Reg,
  dummy_accept_Auth,
  dummy_accept_Balance,
  dummy_accept_ProdList,
  dummy_accept_RecTempl,
  dummy_accept_CreOp,
  dummy_accept_ConOp,
  dummy_accept_OpStatus
};
struct DummyServerAcceptData_t
{
  DummyServerAcceptOperation_t operation;
};
struct DummyServerMsg
{
  enum Action_t {Exit = 0, Accept} action;
  union
  {
    DummyServerAcceptData_t accept;
  } data;
};

void init_DummyServer();
void free_DummyServer();

void DummyServer_exit();
void DummyServer_accept(DummyServerAcceptOperation_t op);

#endif /* DUMMYSERVER_H */

