/* 
 * File:   logs.h
 */

#ifndef LOGS_H
#define LOGS_H

#ifdef __cplusplus

#include <boost/log/trivial.hpp>

#define LOG_INFO BOOST_LOG_TRIVIAL(info)
#define LOG_WARN BOOST_LOG_TRIVIAL(warning)
#define LOG_ERR BOOST_LOG_TRIVIAL(error)

#define LOG_INFO_DATA(header,buf) \
  LOG_INFO << std::endl << header \
    << std::endl << ">>>>>>>>>>>>>>>>" << std::endl \
    << buf \
    << std::endl << "<<<<<<<<<<<<<<<<"  << std::endl

#endif

#ifdef __cplusplus
extern "C"
{
#endif

void init_system_logs();

#ifdef __cplusplus
}
#endif

#endif /* LOGS_H */

