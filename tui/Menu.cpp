#include "Menu.h"

#include "public.h"

#include <cstdlib>
#include <iostream>

#define QUIT_CHAR 'Q'
#define REG_CHAR 'R'
#define AUTH_CHAR 'A'
#define BALANCE_CHAR 'b'
#define PRODUCTSLIST_CHAR 'p'
#define RECEIPTTEMPLATE_CHAR 'r'
#define CREATEOPERATION_CHAR 'n'
#define CONFIRMOPERATION_CNAR 'c'
#define OPERATIONSTATUS_CHAR 's'

static void draw_menu(bool isAuth)
{
  std::cout
    << std::endl
    << "==={ ELpay Terminal Simulator }===" << std::endl
    << "                           " << QUIT_CHAR << ": EXIT" << std::endl
    << "----------------------------------" << std::endl;
  if (isAuth)
    std::cout
      << PRODUCTSLIST_CHAR << ": products list download" << std::endl
      << RECEIPTTEMPLATE_CHAR << ": receipt template download" << std::endl
      << CREATEOPERATION_CHAR << ": create new operation" << std::endl
      << CONFIRMOPERATION_CNAR << ": confirm operation" << std::endl
      << OPERATIONSTATUS_CHAR << ": get operation status" << std::endl
      << BALANCE_CHAR << ": get balance" << std::endl
    ;
  else
    std::cout
      << REG_CHAR << ": registration" << std::endl
      << AUTH_CHAR << ": authorization" << std::endl
    ;
  std::cout
    << "==================================" << std::endl << "Enter char: ";
}

menuAction_t menu_handler(bool isAuth)
{
  draw_menu(isAuth);
  
  char actionChar;
  std::cin >> actionChar;
  switch (actionChar)
  {
  case QUIT_CHAR:
    return menu_Exit;
    
  case REG_CHAR:
    return menu_ELpay_Reg;
    
  case AUTH_CHAR:
    return menu_ELpay_Auth;
    
  case PRODUCTSLIST_CHAR:
    return menu_ELpay_ProductsList;
    
  case RECEIPTTEMPLATE_CHAR:
    return menu_ELpay_ReceiptTemplate;
    
  case CREATEOPERATION_CHAR:
    return menu_ELpay_CreateOperation;
    
  case CONFIRMOPERATION_CNAR:
    return menu_ELpay_ConfirmOperation;
    
  case OPERATIONSTATUS_CHAR:
    return menu_ELpay_OperationStatus;
    
  case BALANCE_CHAR:
    return menu_ELpay_Balance;
  }
  return menu_NoAction;
}
