#include "logs.h"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>

void init_system_logs()
{
  boost::log::add_file_log("ELpayTerminal.Simulator-%Y%m%d-%H%M%S.log");

  boost::log::core::get()->set_filter
  (
    boost::log::trivial::severity >= boost::log::trivial::info
  );
}
