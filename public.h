/* 
 * File:   public.h
 */

#ifndef PUBLIC_H
#define PUBLIC_H

#define ELPAY_SERVER_HOST "m100.el-pay.ru"
#define ELPAY_SERVER_URL_HTTP "http://" ELPAY_SERVER_HOST "/"

#define ELPAY_SERVER_URL ELPAY_SERVER_URL_HTTP

extern bool flag_AutoExit;
extern bool flag_readAction_enable;
extern bool flag_isAuth;

extern bool flag_DummyServer_Enable;
extern bool flag_Server_ResolveAddr;

extern bool flag_stdout_Verbose;

extern const char * Server_json_Addr;

extern const char * DymmyServer_json_Addr;

extern const char * ipPoint_json_Addr;
extern unsigned short ipPoint_json_Port;

extern long elpayRCode;

#endif /* PUBLIC_H */

