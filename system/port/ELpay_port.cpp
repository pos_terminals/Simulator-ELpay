/* 
 * File:   ELpay_port.cpp
 */

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <boost/crc.hpp>

#include <openssl/des.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

#define DES_FUNC(func,key,bytes,count,res) \
{ \
  DES_key_schedule Key; \
  int err = DES_set_key((C_Block*)key, &Key); \
  if (err) \
    return -1; \
  DES_ecb_encrypt((C_Block*)bytes, (C_Block *)res, &Key, DES_ ## func); \
  return 0; \
}

extern "C"
{
  uint16_t elpay_crc16(const char * bytes, size_t count)
  {
    boost::crc_16_type res;
    res.process_bytes(bytes, count);
    return res.checksum();
  }

  int elpay_des_encrypt(const char * key, const char * bytes, size_t count, char * res)
  DES_FUNC(ENCRYPT,key,bytes,count,res);
  
  int elpay_des_decrypt(const char * key, const char * bytes, size_t count, char * res)
  DES_FUNC(DECRYPT,key,bytes,count,res);
  
  size_t elpay_base64_encode(const char * bytes, size_t count, char ** res)
  {
    BIO *bio, *b64;
    BUF_MEM *bptr;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    
    b64 = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    
    BIO_write(b64, bytes, count);
    BIO_flush(b64);
    
    BIO_get_mem_ptr(b64, &bptr);
    *res = (char *)malloc(bptr->length);
    memcpy(*res, bptr->data, bptr->length-1);
    (*res)[bptr->length-1] = 0;
    
    BIO_free_all(b64);
    return bptr->length-1;
  }
  
  static int base64_calcDecodeLength(const char* b64input) 
  {
    int len = strlen(b64input);
    int padding = 0;

    if (b64input[len-1] == '=' && b64input[len-2] == '=') //last two chars are =
      padding = 2;
    else if (b64input[len-1] == '=') //last char is =
      padding = 1;

    return (int)len*0.75 - padding;
  }
  size_t elpay_base64_decode(const char * bytes, size_t count, char ** res)
  {  
    BIO *bio, *b64;

    FILE * stream = fmemopen((void *)bytes, count, "r");
    
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(stream, BIO_NOCLOSE);
    
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
   
    int lenDec = base64_calcDecodeLength(bytes);
    *res = (char *)malloc(lenDec+1);
    
    size_t len = BIO_read(bio, *res, count);
    if (len != lenDec)
      return 0;
    (*res)[len] = '\0';
    
    BIO_free_all(b64);
    fclose(stream);
    return len;
  }
}

//------------------------------------------------------------------------------

#include "public.h"

#include <iostream>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>

#define ELPAY_HTTP_POST "POST " ELPAY_SERVER_URL " HTTP/1.1\r\n"
#define ELPAY_HTTP_JSON_HEADER \
  "Accept: application/jsonrequest\r\n" \
  "Host: " ELPAY_SERVER_HOST "\r\n" \
  "Content-Encoding: identity\r\n" \
  "Content-Type: application/jsonrequest\r\n"
  
#define ELPAY_REG_HTTP_HEADER(MayBeCompress) \
  "User-Agent: M100\r\n" \
  "X-ELpay-Compress: "#MayBeCompress"\r\n"

std::string elpay_http_post(char * data, size_t len,
                     const char *  userHeader)
{
  std::ostringstream httpStringStream;
  httpStringStream << ELPAY_HTTP_POST 
    << ELPAY_HTTP_JSON_HEADER
    << "Content-Length: " << len << "\r\n"
    << ELPAY_REG_HTTP_HEADER("0")
    << userHeader
    << "\r\n";

  std::string httpString = httpStringStream.str();
  httpString.append(data);
    
  return httpString;
}

int elpay_http_read_header_data_idx(const char * buf, size_t len, const char * header_name)
{
  std::string str;
  str.append(buf, len);
  boost::to_upper(str);
  
  size_t HeaderPos = str.find(header_name);
  size_t idx = str.find(":",HeaderPos) + 1;
  
  if (idx >= len)
    return -1;
  
  return idx;
}

long elpay_http_content_len(const char * buf, size_t len)
{
  size_t idx = elpay_http_read_header_data_idx(buf,len,"CONTENT-LENGTH");
  if (idx >= len)
    return -1;
  
  return std::stol(&buf[idx]);
}

int elpay_http_content_idx(const char * buf, size_t len)
{
  std::string httpString;
  httpString.append(buf, len);
  
  size_t idx = httpString.find("\r\n\r\n");
  idx += 4;
  if (idx >= len)
    return -1;
  return idx;
}
