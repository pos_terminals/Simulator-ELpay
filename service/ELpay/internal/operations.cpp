/* 
 * File:   operations.cpp
 */

#include "transport.h"
#include "operations.h"

void set_elpay_mvi(ELpayMVI_t* mvi)
{
    mvi->model = "Simulator";
    mvi->os_ver = "Win7";
    mvi->imei = "123456789098765";
    mvi->phone_num = "+79998887766";
    mvi->lac = "65535";
    mvi->cid = "65535";  
}

const char elpay_POS_OS_ver[] = "1.0.0";
const char elpay_App_ver[] = "1.0.0";

ELpayOperationStatus_t elpay_registration(ELpayTerminalData_t* ThisData, long code)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  
  if (init_tcp_socket(&Socket))
  {
    ELpayRegData_t data;
    snprintf(data.code,ELPAY_JSON_KEY_BUFSIZE, "%d", code);
    set_elpay_mvi(&data.mvi);
    data.pos_os_ver = elpay_POS_OS_ver;
    data.app_ver = elpay_App_ver;
    
    if (elpay_reg_req_send(&Socket, &data))
    {
      if (!elpay_reg_resp_get(&Socket, ThisData, data.code))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_authorization(ELpayTerminalData_t* ThisData)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayAuthData_t data;
    data.termId = ThisData->id;
    data.login = ThisData->login;
    data.password = ThisData->password;
    set_elpay_mvi(&data.mvi);
    data.pos_os_ver = elpay_POS_OS_ver;
    data.app_ver = elpay_App_ver;
    
    if (elpay_auth_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_auth_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_get_products_list(ELpayTerminalData_t* ThisData)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayProductsListData_t data;
    data.base.termId = ThisData->id;
    data.base.session = ThisData->session;
    data.base.app_ver = elpay_App_ver;
    
    if (elpay_products_list_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_products_list_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_get_receipt_template(ELpayTerminalData_t* ThisData, const char * name)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayReceiptTemplateData_t data;
    data.base.termId = ThisData->id;
    data.base.session = ThisData->session;
    data.base.app_ver = elpay_App_ver;
    data.name = name;
    
    if (elpay_receipt_template_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_receipt_template_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_create_operation(ELpayTerminalData_t* ThisData, long pid)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayCreateOperationData_t data;
    data.base.termId = ThisData->id;
    data.base.session = ThisData->session;
    data.base.app_ver = elpay_App_ver;
    data.commision = 0;
    data.msisdn = "0970002000";
    data.pid = pid;
    data.act = 1;
    data.nominal = 1;
    
    if (elpay_create_operation_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_create_operation_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_confirm_operation(ELpayTerminalData_t* ThisData)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayConfirmOperationData_t data;
    data.base.termId = ThisData->id;
    data.base.session = ThisData->session;
    data.base.app_ver = elpay_App_ver;
    data.tranId = ThisData->tranId;
    
    if (elpay_confirm_operation_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_confirm_operation_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_operation_status(ELpayTerminalData_t* ThisData)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayOperationStatusData_t data;
    data.base.termId = ThisData->id;
    data.base.session = ThisData->session;
    data.base.app_ver = elpay_App_ver;
    data.tranId = ThisData->tranId;
    
    if (elpay_operation_status_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_operation_status_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}

ELpayOperationStatus_t elpay_get_balance(ELpayTerminalData_t* ThisData)
{
  ELpayOperationStatus_t res = elpay_status_OK;
  
  io_service Service;
  ip::tcp::socket Socket(Service);
  if (init_tcp_socket(&Socket))
  {
    ELpayBalanceData_t data;
    data.base.termId = ThisData->id;
    data.base.session = ThisData->session;
    data.base.app_ver = elpay_App_ver;
    
    if (elpay_balance_req_send(&Socket, &data, ThisData->sign_key))
    {
      if (!elpay_balance_resp_get(&Socket, ThisData))
        res = elpay_status_ResponseFailure;
    }
    else
      res = elpay_status_RequestFailure;
  }
  else
    res = elpay_status_GeneralFailure;
  close_tcp_socket(&Socket);
  return res;
}
