/* 
 * File:   json_ELpay.c
 */

#include "ELpay_port.h"
#include "protocol.h"
#include "json.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

char * elpay_sign(const char * key, char * packet)
{
	char * sig = (char *)malloc(9);
	memset(sig, '\0', 9);
	int p = 0;
	size_t len = strlen(packet);
	int i, j;
	for (i = 0; i < (len + 7) / 8; i++) {
		if ((i == (((len + 7) / 8) - 1)) && ((len % 8) > 0)) 
    {
			for (j = 0; j < (len % 8); j++)
				sig[j] ^= packet[p++];
			for (j = len % 8; j < 8; j++)
				sig[j] ^= 0;
			elpay_des_encrypt(key, sig, 8, sig);
			break;
		}
		for (j = 0; j < 8; j++)
			sig[j] ^= packet[p++];
		elpay_des_encrypt(key, sig, 8, sig);
	}
	char *res;
	elpay_base64_encode(sig, 8, &res);
	free(sig);
	return res;
}


#define GEN_and_CHECK(func)                                          \
{                                                                    \
  if (!func)                                                         \
  {                                                                  \
    free_jsonGen(&Gen);                                         \
    return 0;                                                        \
  }                                                                  \
}

static inline void * write_mvi(ELpayMVI_t * mvi, char * buf, size_t size)
{
  snprintf(buf, size,
    /*Model*/"%s" /*os_ver*/";%s" 
    /*imei*/";%s" /*phone_num*/";%s" /*lac*/";%s" /*cid*/";%s",
    mvi->model, mvi->os_ver,
    mvi->imei, mvi->phone_num, mvi->lac, mvi->cid);
}

size_t elpay_reg_req_gen(ELpayRegData_t* data,
  char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "rcode", 
      elpay_crc16(data->code, strlen(data->code))));

    JSON_CHAR tmp_buf[1024];
    write_mvi(&data->mvi, tmp_buf, sizeof(tmp_buf));
    GEN_and_CHECK(json_gen_write_str(&Gen, "m-v-i", tmp_buf));

    GEN_and_CHECK(json_gen_write_str(&Gen, "pos", data->pos_os_ver));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1000));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->app_ver));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;
}

int elpay_auth_req_gen(ELpayAuthData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1));

    GEN_and_CHECK(json_gen_write_str(&Gen, "login", data->login));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "pwd", data->password));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "pos", data->pos_os_ver));

    JSON_CHAR tmp_buf[1024];
    write_mvi(&data->mvi, tmp_buf, sizeof(tmp_buf));
    GEN_and_CHECK(json_gen_write_str(&Gen, "m-v-i", tmp_buf));
 
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->app_ver));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

int elpay_products_list_req_gen(ELpayProductsListData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1004));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "last-prod-ver", "0"));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

int elpay_receipt_template_req_gen(ELpayReceiptTemplateData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1016));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "template-name", data->name));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "last-prod-ver", "0"));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

int elpay_create_operation_req_gen(ELpayCreateOperationData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 10));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "msisdn", data->msisdn));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "pid", data->pid));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "act", data->act));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "nominal", data->nominal));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "comm", data->commision));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

int elpay_confirm_operation_req_gen(ELpayConfirmOperationData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 11));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "tran-id", data->tranId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

int elpay_operation_status_req_gen(ELpayOperationStatusData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 15));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "tran-id", data->tranId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

int elpay_balance_req_gen(ELpayBalanceData_t* data,
    char * buf, size_t size)
{
  jsonGen_t Gen;
  init_jsonGen(&Gen);
  jsonGen_clear(&Gen);
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1017));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size);
  
  free_jsonGen(&Gen);  
  return len;  
}

//------------------------------------------------------------------------------

#define JSON_PATH_NULL (const char *)0

static const char * elpayPath_Err[] = {"error", JSON_PATH_NULL};
static const char * elpayPath_ErrText[] = {"error-text", JSON_PATH_NULL};

#define PARSE_RETURN(res)\
{ \
  yajl_tree_free(node); \
  return res; \
}

#define PARSE_ERR_HANDLER(val,errHeader)\
if (!(val)) \
{ \
  snprintf(errbuf,errbufSize, "%s", errHeader); \
  PARSE_RETURN(0); \
}

#define PARSE_BASE \
  errbuf[0] = 0; \
  yajl_val node = yajl_tree_parse(buf, errbuf, errbufSize); \
  if (node == NULL) \
  { \
    if (!strlen(errbuf)) snprintf(errbuf,errbufSize, "unknown error"); \
    PARSE_RETURN(0); \
  }

#define PARSE_CHECK_ERROR \
{ \
  yajl_val errVal = yajl_tree_get(node, elpayPath_Err, yajl_t_number); \
  if (errVal) \
  { \
    yajl_val errTextVal = yajl_tree_get(node, elpayPath_ErrText, yajl_t_string); \
    if (errTextVal) \
      snprintf(errbuf,errbufSize, "error #%d: %s", \
        YAJL_GET_INTEGER(errVal), \
        YAJL_GET_STRING(errTextVal)); \
    PARSE_RETURN(0); \
  } \
}

static const char * elpayPath_RegMK[] = {"mk", JSON_PATH_NULL};
static const char * elpayPath_RegSK[] = {"sk", JSON_PATH_NULL};
static const char * elpayPath_RegEncCV[] = {"enccv", JSON_PATH_NULL};
static const char * elpayPath_RegTerm[] = {"term", JSON_PATH_NULL};

int elpay_reg_resp_parse(const char * buf, 
    ELpayKey_t regcode, ELpayTerminalData_t * res,
  char * errbuf, size_t errbufSize)
{
  PARSE_BASE;
  PARSE_CHECK_ERROR;
  
  yajl_val mkVal = yajl_tree_get(node, elpayPath_RegMK, yajl_t_string);
  PARSE_ERR_HANDLER(mkVal,"MK read failure");
  
  yajl_val skVal = yajl_tree_get(node, elpayPath_RegSK, yajl_t_string);
  PARSE_ERR_HANDLER(skVal,"SK read failure");
  
  yajl_val cvVal = yajl_tree_get(node, elpayPath_RegEncCV, yajl_t_string);
  PARSE_ERR_HANDLER(cvVal,"CV read failure");

  yajl_val termVal = yajl_tree_get(node, elpayPath_RegTerm, yajl_t_string);
  PARSE_ERR_HANDLER(termVal,"Term");
  snprintf(res->id,ELPAY_JSON_TERM_ID_BUFSIZE, "%s", YAJL_GET_STRING(termVal));

  char * mk0, * sk0, * cv0;
  
  size_t len;
  len = elpay_base64_decode(YAJL_GET_STRING(mkVal), ELPAY_JSON_KEYenc_SIZE, &mk0);
  PARSE_ERR_HANDLER(len==ELPAY_JSON_KEYSIZE,"MK decode failure");
  len = elpay_base64_decode(YAJL_GET_STRING(skVal), ELPAY_JSON_KEYenc_SIZE, &sk0);
  PARSE_ERR_HANDLER(len==ELPAY_JSON_KEYSIZE,"SK decode failure");
  len = elpay_base64_decode(YAJL_GET_STRING(cvVal), ELPAY_JSON_KEYenc_SIZE, &cv0);
  PARSE_ERR_HANDLER(len==ELPAY_JSON_KEYSIZE,"CV decode failure");
  
  ELpayKey_t mk, cv;
  
  elpay_des_decrypt(regcode, mk0, ELPAY_JSON_KEYSIZE, mk);
  elpay_des_decrypt(mk, sk0, ELPAY_JSON_KEYSIZE, res->sign_key);
  elpay_des_decrypt(res->sign_key, cv0, ELPAY_JSON_KEYSIZE, cv);
  
  free(mk0);
  free(sk0);
  free(cv0);
  
  int64_t * Check = (int64_t *)cv;
  PARSE_ERR_HANDLER(!(*Check), "check failure");
  
  PARSE_RETURN(1);
}

static const char * elpayPath_AuthSession[] = {"session", JSON_PATH_NULL};
static const char * elpayPath_AuthVersion[] = {"version", JSON_PATH_NULL};

int elpay_auth_resp_parse(const char * buf, 
    ELpayTerminalData_t * res,
    char * errbuf, size_t errbufSize)
{
  PARSE_BASE;
  PARSE_CHECK_ERROR;
  
  yajl_val seesionVal = yajl_tree_get(node, elpayPath_AuthSession, yajl_t_string);
  PARSE_ERR_HANDLER(seesionVal,"SessionId read failure");
  snprintf(res->session,ELPAY_JSON_SESSION_ID_BUFSIZE, "%s", YAJL_GET_STRING(seesionVal));
  
  yajl_val versionVal = yajl_tree_get(node, elpayPath_AuthVersion, yajl_t_string);
  PARSE_ERR_HANDLER(seesionVal,"Version read failure");
  
  PARSE_RETURN(1);
}

static const char * elpayPath_TranId[] = {"tran-id", JSON_PATH_NULL};

int elpay_create_operation_resp_parse(const char * buf, 
    ELpayTerminalData_t * res,
    char * errbuf, size_t errbufSize)
{
  PARSE_BASE;
  PARSE_CHECK_ERROR;
  
  yajl_val tranIdVal = yajl_tree_get(node, elpayPath_TranId, yajl_t_number);
  PARSE_ERR_HANDLER(tranIdVal,"TranId read failure");
  res->tranId = YAJL_GET_INTEGER(tranIdVal);
  
  PARSE_RETURN(1);
}
