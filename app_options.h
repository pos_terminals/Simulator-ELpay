#ifndef APP_OPTIONS_H
#define APP_OPTIONS_H

#include "tui/Menu.h"

enum appCommand_t {app_EXIT = 0, app_CONTINUE = 1};
appCommand_t app_options_handler(int argc, char** argv, menuAction_t* menuAction);

#endif // APP_OPTIONS_H